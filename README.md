# Unity IAP Samples

For use with Unity 5.3 or higher

Illustrates usage patterns for Unity IAP.

## About Unity IAP

* Provides in-app purchase (IAP) functionality to Unity applications. It enables the purchase of digital products by way of a host device's Billing System from that application's Publisher. To learn more about Unity IAP visit https://unity3d.com/services/analytics/iap and read documentation at http://docs.unity3d.com/Manual/UnityIAP.html.


# Common Setup

Required steps to run any of the included samples: install the most up-to-date Stores and bind to a Unity Services Account.

1. Enable Unity IAP
    1. In Window > Services > In-App Purchasing:
    1. Switch "on" Unity IAP for the sample
    1. Import Unity IAP stores package
1. Build the sample


# FAQ

* Q: I see an error message: "The type or namespace name Purchasing does not exist in the namespace UnityEngine. Are you missing a using directive or an assembly reference?"
    * A: Follow the Setup instructions, above, to switch "on" Unity IAP.
* Q: I see an error message: "The type or namespace name IAppleExtensions could not be found. Are you missing a using directive or an assembly reference?"
    * A: Follow the Setup instructions, above, to import the Unity IAP stores package.


# Sample Catalog

* [UnityIAPDemo](UnityIAPDemo) - Uses UnityEngine.UI to demonstrate product enumeration, purchase, and purchase restoration for Apple
* [UnityIAPSingletonDemo](UnityIAPSingletonDemo) - Refactors UnityIAPDemo with a singleton initialization design pattern, demonstrating Scene reload-safety. Unity IAP does not currently support re-initialization. It's recommended to use a singleton approach for games having more than one scene.


# What license are the Unity IAP Samples shipped under?

This is released under the [MIT/X11 license](LICENSE).

This means that you pretty much can customize and embed it in any software under any license without any other constraints than preserving the copyright and license information while adding your own copyright and license information.

You can keep the source to yourself or share your customized version under the same MIT license or a compatible license.

If you want to contribute patches back, please keep it under the unmodified MIT license so it can be integrated in future versions and shared under the same license.